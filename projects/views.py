from rest_framework import generics

from .models import Project, User
from .serializers import ProjectWithUsersSerializer, UserWithProjectsSerializer


class ProjectListCreate(generics.ListCreateAPIView):
    queryset = Project.objects.order_by('-created_at')
    serializer_class = ProjectWithUsersSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserWithProjectsSerializer
