import React, {Component, Fragment} from "react";
import {render} from "react-dom";
import {Card, ListGroup, ListGroupItem, Container, Row, Navbar} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


function User({user}) {
    return (
        <ListGroupItem>
            {user.first_name} {user.last_name}
        </ListGroupItem>
    );
}

function Project({project}) {
    return (
        <Row className='my-3'>
            <Card style={{ width: '18rem' }}>
                <Card.Body>
                    <Card.Title>{project.name}</Card.Title>
                    <ListGroup className="list-group-flush">
                    {project.users.map(user => {
                        return (
                            <User user={user} key={"project_" + project.id + "_user_" + user.id}/>
                        );
                    })}
                    </ListGroup>
                </Card.Body>
            </Card>
        </Row>
    );
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loaded: false,
            placeholder: "Loading"
        };
    }

    componentDidMount() {
        fetch("api/project")
            .then(response => {
                if (response.status > 400) {
                    return this.setState(() => {
                        return {placeholder: "Something went wrong!"};
                    });
                }
                return response.json();
            })
            .then(data => {
                this.setState(() => {
                    return {
                        data,
                        loaded: true
                    };
                });
            });
    }

    render() {
        return (
            <Fragment>
                <Navbar bg="primary" variant="dark">
                    <Navbar.Brand>Projects</Navbar.Brand>
                </Navbar>
                <Container>
                    {this.state.data.map(project => {
                        return (
                            <Project key={"project_" + project.id} project={project}/>
                        );
                    })}
                </Container>
            </Fragment>
        );
    }
}

export default App;

const container = document.getElementById("app");
render(<App/>, container);
